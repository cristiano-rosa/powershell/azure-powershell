# THIS SCRIPT IS GOING TO DEPLOY A RESOURCE GROUP AND A VIRTUAL NETWORK IN AZURE.
# YOU CAN ALSO ENABLE THE DEPLOYMENT OF NETWORK INTERFACE, PUBLIC IP, NETWORK SECURITY GROUP AND VIRTUAL MACHINE.

# 1) DEFINING THE VARIABLES:
# 1.1) PLEASE DEFINE THE VARIABLES UNDER PARAM SECTION. 
# 1.2) DEFINE THE INCOMING FIREWALL RULES UNDER RULES_INBOUND SECTION.
# 2) USING ENVIRONMENT VARIABLES:
# 2.1) PLEASE READ THE ENVIRONMENT VARIABLES SECTION IN CASE YOU ARE GOING TO USE ENV VARIABLES TO CONNECT TO AZURE.
# 2.2) IF NOT USING ENV VARIABLES OR IF YOUR ENIRONMENT IS ALREADY CONNECTED TO AZURE, COMMENT THE CONNECT TO AZURE SECTION.


param (
    # RESOURCE GROUP AND REGION DEFINITION
    [string]$ResourceGroupName = "Bere-VMs-1", # Name of your resource group
    [string]$Location = "North Europe", # Region where you want to deploy the resources
    
    # VIRTUAL NETWORK DEFINITION
    [string]$VirtualNetworkName = "bere-virtual-network-1", # Name of your virtual network
    [string]$VirtualNetworkAddessSpace = "10.0.0.0/16", # CIDR of your virtual network
    [string]$SubnetName = "bere-subnet-1", # Name of your subnet
    [string]$SubnetAddressSpace = "10.0.0.0/24", # CIDR of your subnet
    
    # NETWORK INTERFACE DEFINITION
    [string]$NetworkInterfaceName = "bere-network-interface-1", # Name of your network interface
    [bool]$Deploy_Network_Interface = $true, # $true or $false depending on whether you want to deploy a network interface
    
    # PUBLIC IP DEFINITION
    [string]$PublicIPAddressName = "bere-public-ip-1", # Name of your public ip
    [string]$PublicIPSKU = "Standard", # Basic or Standard 
    [string]$PublicIPAllocation = "Static", # Dynamic or Static
    [bool]$Deploy_Public_IP = $true, # $true or $false depending on whether you want to deploy a public ip
    
    # NETWORK SECURITY GROUP DEFINITION
    [bool]$Deploy_Security_Group = $true, # $true or $false depending on whether you want to deploy a network security group to your subnet
    [string]$NetworkSecurityGroupName = "bere-nsg-1", # Name of your network security group

    # VIRTUAL MACHINE DEFINITION
    [bool]$Deploy_Virtual_Machine = $true, # $true or $false depending on whether you want to deploy a network security group to your subnet
    [string]$VirtualMachineName = "bere-vm-1", # Name of your virtual machine
    [string]$VirtualMachineSize = "Standard_B1s", # Enter a VM Size to support the workload that you want to run. "Get-AzVMSize -Location $Location" to see available sizes.
    [System.Management.Automation.PSCredential]$VirtualMachineCredentials = $null, # VM credentials is by default set to null, so it will prompt the user to enter the VM credentials
    
    # OPERATING SYSTEM DEFINITION
    [string]$VirtualMachineOS = "Windows", # Windows or Linux
    [string]$VirtualMachineOSPublisher = "MicrosoftWindowsServer", # Examples: MicrosoftWindowsServer, OpenLogic, RedHat.
    [string]$VirtualMachineOSOffer = "WindowsServer", # Examples: WindowsServer, CentOS, RHEL.
    [string]$VirtualMachineOSSku = "2019-Datacenter", # To obtain SKUs, use the Get-AzVMImageSku cmdlet. Examples: 2019-datacenter, 7_9, 9_2.
    [string]$VirtualMachineOSVersion = "latest", # Version of the VirtualMachineOSSku which was selected.

    # DETAILED OUTPUT DEFINITION
    [bool]$Detailed_Output = $true # $true or $false depending on whether you want to enable verbose in the output
)


# VIRTUAL MACHINE CREDENTIALS
# Check if $VirtualMachineCredentials is $null and prompt for credentials when this parameter is not provided.
if (-not $VirtualMachineCredentials) {
    $VirtualMachineCredentials = Get-Credential -Message "Please provide the VM credentials (user Admin and password)."
}


# INBOUND NETWORK SECURITY GROUP RULES DEFINITION
# The following rules are only deployed if $Deploy_Security_Group = $true
# In this section you can add additional incoming rules to connect from internet to your subnet where your VM is:
$Rules_Inbound = @( 
    @{
        Enabled = $true
        Name = "Allow-RDP"
        Priority = 100
        SourceAddress = "*"
        DestinationPort = 3389
        Protocol = "Tcp"
        Access = "Allow"
    },
    @{
        Enabled = $true
        Name = "Allow-HTTP"
        Priority = 101
        SourceAddress = "*"
        DestinationPort = 80
        Protocol = "Tcp"
        Access = "Allow"
    },
    @{
        Enabled = $true
        Name = "Allow-HTTPS"
        Priority = 102
        SourceAddress = "*"
        DestinationPort = 443
        Protocol = "Tcp"
        Access = "Allow"
    }
)


# ENVIRONMENT VARIABLES
# For security reasons we are not going to hardcode $AppId, $AppSecret and $TenantID in this script,
# instead you have to set these three environment variables outside of this code in the OS/environment where you are going to run it.
# So make sure you set the env variables AZ_APP_ID, AZ_APP_SECRET and AZ_TENANT_ID in your environment first, before runnning this script.
# Example on how to set environment variables:
#   Windows: 
#     $env:AZ_APP_SECRET = "your_secret_here"
#     $env:AZ_APP_ID = "your_app_id_here" 
#     $env:AZ_TENANT_ID = "your_tenant_id_here" 
#   Linux and macOS:
#     export AZ_APP_SECRET="your_secret_here"
#     export AZ_APP_ID="your_app_id_here"
#     export AZ_TENANT_ID="your_tenant_id_here"
# If you don't want to use these env variables to run this script, or if your environment is already connected to Azure, make sure you comment the section "CONNECT TO AZURE".


# CONNECT TO AZURE - COMMENT THIS SECTION IF YOUR ENVIRONMENT IS ALREADY CONNECTED TO AZURE OF IF YOU ARE NOT GOING TO USE ENV VARIABLES
$AppId = [System.Environment]::GetEnvironmentVariable('AZ_APP_ID')
$AppSecret = [System.Environment]::GetEnvironmentVariable('AZ_APP_SECRET')
$SecureSecret = $AppSecret | ConvertTo-SecureString -AsPlainText -Force
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $AppId,$SecureSecret
$TenantID = [System.Environment]::GetEnvironmentVariable('AZ_TENANT_ID')
Connect-AzAccount -ServicePrincipal -Credential $Credential -Tenant $TenantID


# CHECK IF THE RESOURCE GROUP ALREADY EXISTS
if(Get-AzResourceGroup -Name $ResourceGroupName -Location $Location -ErrorAction SilentlyContinue)
{
    $ResourceGroup=Get-AzResourceGroup -Name $ResourceGroupName -Location $Location
    Write-Host "`n********** The $($ResourceGroup.ResourceGroupName) Resource Group already exists.`n" 
}

else {

    # CREATE RESOURCE GROUP
    $ResourceGroup = New-AzResourceGroup -Name $ResourceGroupName -Location $Location
    Write-Host "`n********** The Resource Group $($ResourceGroup.ResourceGroupName) was created and its Provisioning State is $($ResourceGroup.ProvisioningState).`n"
    if ($Detailed_Output -eq $true) {
        $ResourceGroup
    }

}


# CREATE SUBNET
$Subnet = New-AzVirtualNetworkSubnetConfig -Name $SubnetName -AddressPrefix $SubnetAddressSpace
Write-Host "`n********** The Subnet $($Subnet.Name) with CIDR $($Subnet.AddressPrefix) was created.`n"
if ($Detailed_Output -eq $true) {
    $Subnet # at this point the Subnet.Id is empty since it was not assigned to a virtual network yet
}


# CREATE VIRTUAL NETWORK
$VirtualNetwork = New-AzVirtualNetwork -Name $VirtualNetworkName -ResourceGroupName $ResourceGroupName -Location $Location -AddressPrefix $VirtualNetworkAddessSpace -Subnet $Subnet
$AddressPrefixes = $VirtualNetwork.AddressSpace.AddressPrefixes
$FirstAddressPrefix = $AddressPrefixes[0]
Write-Host "`n********** The Virtual Network $($VirtualNetwork.Name) with CIDR $($FirstAddressPrefix) was created and it is in Provisioning State $($VirtualNetwork.ProvisioningState).`n"
if ($Detailed_Output -eq $true) {
    $VirtualNetwork 
}


# IF ENABLED THEN CREATE NETWORK INTERFACE
if ($Deploy_Network_Interface -eq $true) {
    # GET THE SUBNET ID
    # at this point the Subnet.Id is not empty anymore since it was assigned to a virtual network
    $Subnet = Get-AzVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $VirtualNetwork

    # CREATE NETWORK INTERFACE
    $NetworkInterface = New-AzNetworkInterface -Name $NetworkInterfaceName -ResourceGroupName $ResourceGroupName -Location $Location -Subnet $Subnet
    $PrivateIpAddress = $NetworkInterface.IpConfigurations[0].PrivateIpAddress
    Write-Host "`n********** The Network Interface $($NetworkInterface.Name) was created and it is in Provisioning State $($NetworkInterface.ProvisioningState) with internal IP address $($PrivateIpAddress).`n"
    if ($Detailed_Output -eq $true) {
        $NetworkInterface 
    }

}


# IF ENABLED THEN CREATE PUBLIC IP
if ($Deploy_Public_IP -eq $true) {
    
    # GET THE IPCONFIG OF THE NETWORK INTERFACE
    $IpConfig = Get-AzNetworkInterfaceIpConfig -NetworkInterface $NetworkInterface

    # CREATE PUBLIC IP
    $PublicIPAddress = New-AzPublicIpAddress -Name $PublicIPAddressName -ResourceGroupName $ResourceGroupName -Location $Location -Sku $PublicIPSKU -AllocationMethod $PublicIPAllocation
    Write-Host "`n********** The Public IP $($PublicIPAddress.Name) was created and it is in Provisioning State $($PublicIPAddress.ProvisioningState) with IP address $($PublicIPAddress.IpAddress).`n"
    if ($Detailed_Output -eq $true) {
        $PublicIPAddress 
    }

    # UPDATE THE IP CONFIGURATION FOR THE NETWORK INTERFACE
    $NetworkInterface | Set-AzNetworkInterfaceIpConfig -PublicIpAddress $PublicIPAddress -Name $IpConfig.Name | Out-Null

    # UPDATE THE NETWORK INTERFACE
    $NetworkInterface | Set-AzNetworkInterface | Out-Null
    Write-Host "`n********** The Network Interface $($NetworkInterface.Name) was created and it is in Provisioning State $($NetworkInterface.ProvisioningState) and was assigned to the public IP $($PublicIPAddress.IpAddress).`n"
    if ($Detailed_Output -eq $true) {
        $NetworkInterface 
    }

}


# IF ENABLED THEN CREATE NETWORK SECURITY GROUP
if ($Deploy_Security_Group -eq $true) {
    
    # INITIALIZE AN EMPTY ARRAY TO HOLD SECURITY RULE OBJECTS
    $SecurityRules_Inbound = @()

    # LOOP THROUGH EACH INBOUND RULE AND CREATE SECURITY RULES IF ENABLED
    foreach ($Rule in $Rules_Inbound) {
        if ($Rule.Enabled) {
            $SecurityRule = New-AzNetworkSecurityRuleConfig -Name $Rule.Name -Description $Rule.Name -Access $Rule.Access -Protocol $Rule.Protocol -Direction Inbound -Priority $Rule.Priority `
            -SourceAddressPrefix $Rule.SourceAddress -SourcePortRange * -DestinationAddressPrefix * -DestinationPortRange $Rule.DestinationPort
            $SecurityRules_Inbound += $SecurityRule  # Add the new security rule to the array
        }
    }

    # CREATE NETWORK SECURITY GROUP
    $NetworkSecurityGroup = New-AzNetworkSecurityGroup -Name $NetworkSecurityGroupName -ResourceGroupName $ResourceGroupName -Location $Location -SecurityRules $SecurityRules_Inbound
    Write-Host "`n********** The Network Security Group $($NetworkSecurityGroup.Name) was created and it is in ProvisioningState $($NetworkSecurityGroup.ProvisioningState).`n"
    $NetworkSecurityGroup

    # UPDATE SUBNET CONFIGURATION
    $SubnetConfig = Set-AzVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $VirtualNetwork -NetworkSecurityGroup $NetworkSecurityGroup -AddressPrefix $SubnetAddressSpace
    Write-Host "`n********** The Subnet $($SubnetConfig.Subnets[0].Name) is in ProvisioningState $($SubnetConfig.Subnets[0].ProvisioningState) and was updated to use the NSG $($SubnetConfig.Subnets[0].NetworkSecurityGroup.Id.Split('/')[-1]).`n"
    if ($Detailed_Output -eq $true) {
        $SubnetConfig
    }

    # UPDATE THE VIRTUAL NETWORK
    $VirtualNetwork | Set-AzVirtualNetwork | Out-Null
    Write-Host "`n********** The Virtual Network $($VirtualNetwork.Name) was updated and is in Provisioning State $($VirtualNetwork.ProvisioningState).`n"
    if ($Detailed_Output -eq $true) {
        $VirtualNetwork 
    }

}


# IF ENABLED THEN CREATE VIRTUAL MACHINE
if ($Deploy_Virtual_Machine -eq $true) {

    # CREATE VIRTUAL MACHNE
    # The New-AzVM cmdlet creates a virtual machine in Azure. 
    # However the New-AzVMConfig creates a virtual machine object where you can set different properties of the virtual machine object.
    $VirtualMachine = New-AzVMConfig -Name $VirtualMachineName -VMSize $VirtualMachineSize

    # SET THE OPERATING SYSTEM
    if ($VirtualMachineOS -eq "Windows") {
        $VirtualMachine = Set-AzVMOperatingSystem -VM $VirtualMachine -ComputerName $VirtualMachineName -Credential $VirtualMachineCredentials -Windows
    }
    elseif ($VirtualMachineOS -eq "Linux") {
        $VirtualMachine = Set-AzVMOperatingSystem -VM $VirtualMachine -ComputerName $VirtualMachineName -Credential $VirtualMachineCredentials -Linux
    }
    else {
        Write-Host "Invalid VirtualMachineOS value. Please specify 'Windows' or 'Linux'."
        exit
    }

    # SET THE SOURCE IMAGE
    # Reference: https://learn.microsoft.com/en-us/azure/virtual-machines/windows/cli-ps-findimage
    $VirtualMachine = Set-AzVMSourceImage -VM $VirtualMachine -PublisherName $VirtualMachineOSPublisher -Offer $VirtualMachineOSOffer -Skus $VirtualMachineOSSku -Version $VirtualMachineOSVersion

    # ADD NETWORK INTERFACE TO VIRTUAL MACHINE
    $VirtualMachine = Add-AzVMNetworkInterface -VM $VirtualMachine -Id $NetworkInterface.Id

    # BOOT DIAGNOSTICS OF VIRTUAL MACHINE
    $VirtualMachine = Set-AzVMBootDiagnostic -Disable -VM $VirtualMachine

    # CREATE VIRTUAL MACHINE
    $NewVirtualMachine = New-AzVM -ResourceGroupName $ResourceGroupName -Location $Location -VM $VirtualMachine

    Write-Host "`n********** The New Virtual Machine $($NewVirtualMachine.Name) was created and its success status code is $($NewVirtualMachine.IsSuccessStatusCode).`n"
    if ($Detailed_Output -eq $true) {
        $NewVirtualMachine 
    }

}